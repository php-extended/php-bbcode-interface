<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-bbcode-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Bbcode;

use Stringable;

/**
 * BbcodeVisitorInterface interface file.
 * 
 * This interface specifies an object that can walk the Bbcode Node Tree.
 * 
 * @author Anastaszor
 */
interface BbcodeVisitorInterface extends Stringable
{
	
	/**
	 * Visits the given collection node.
	 * 
	 * @param BbcodeCollectionNodeInterface $collection
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitCollection(BbcodeCollectionNodeInterface $collection);
	
	/**
	 * Visits the given single node.
	 * 
	 * @param BbcodeSingleNodeInterface $single
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitSingle(BbcodeSingleNodeInterface $single);
	
}
