<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-bbcode-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Bbcode;

/**
 * BbcodeSingleNodeInterface interface file.
 * 
 * This interface specifies all the actions that every node that has no
 * children should be able to do.
 * 
 * @author Anastaszor
 */
interface BbcodeSingleNodeInterface extends BbcodeAbstractNodeInterface
{
	
	/**
	 * Gets the value of this node. The value of the node is the data between
	 * the begin tag and the end tag, like "https://www.example.com" in 
	 * [url]https://www.example.com[/url].
	 * 
	 * @return string
	 */
	public function getValue() : string;
	
	/**
	 * Gets whether this node is autoclosed. If true, only the begin tag should
	 * be represented as bbcode.
	 * 
	 * @return boolean
	 */
	public function isAutoclosed() : bool;
	
}
