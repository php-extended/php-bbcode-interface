<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-bbcode-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Bbcode;

use Countable;
use Iterator;

/**
 * BbcodeCollectionNodeInterface interface file.
 * 
 * This interface specifies all the actions that every node that may have
 * children should be able to do.
 * 
 * @author Anastaszor
 * @extends \Iterator<int, BbcodeAbstractNodeInterface>
 */
interface BbcodeCollectionNodeInterface extends BbcodeAbstractNodeInterface, Countable, Iterator
{
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::current()
	 */
	public function current() : BbcodeAbstractNodeInterface;
	
}
