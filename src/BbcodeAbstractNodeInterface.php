<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-bbcode-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Bbcode;

use Stringable;

/**
 * BbcodeAbstractNodeInterface interface file.
 * 
 * This interface represents the root of a composite pattern.
 * 
 * @author Anastaszor
 * @abstract
 */
interface BbcodeAbstractNodeInterface extends Stringable
{
	
	/**
	 * Gets the name of this node. The name of the node is the string between
	 * the brackets, like "center" in [center]...[/center].
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the property of this node, if any. The property of a node is the
	 * string after the equals in the head tag, like "2" in [size=2]...[/size].
	 * 
	 * @return string
	 */
	public function getProperty() : string;
	
	/**
	 * Gets whether this node is empty.
	 * 
	 * @return boolean
	 */
	public function isEmpty() : bool;
	
	/**
	 * Gets whether this node is equals to the other given node.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean
	 */
	public function equals($object) : bool;
	
	/**
	 * Makes this node be visited by the given visitor.
	 * 
	 * @param BbcodeVisitorInterface $visitor
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(BbcodeVisitorInterface $visitor);
	
}
