<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-bbcode-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Bbcode;

use PhpExtended\Parser\ParserInterface;

/**
 * BbcodeParserInterface interface file.
 * 
 * This class represents a parser for bbcode data.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Parser\ParserInterface<BbcodeCollectionNodeInterface>
 */
interface BbcodeParserInterface extends ParserInterface
{
	
	// nothing to add
	// php does not accepts covariant return types between interfaces
	
}
